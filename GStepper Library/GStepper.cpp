/*
  GStepper.cpp - by Reilly Raab
  The G stands for "Gaucho" - written at UCSB
  
  Inspired by Stepper library by Tom Igoe
  and AccelStepper by Mike McCauley
  
  LICENCED UNDER GNU GPL V2.0
  CONSPICUOUS NOTICE:
  NO EXPRESSED OR IMPLIED WARRANTY, COMES AS IS
  USE AT YOUR OWN RISK, ETC.
  FREE TO MODIFY AND REDISTRIBUTE, BUT MUST
  REMAIN UNDER GPL V2 AND PROVIDE COPY OF LICENCE

  Note, we measure everything in half-steps, since that's the smallest
  change in position we can easily get our system to make.

  FOR 4 WIRES:
  (Bipolar or Unipolar. For Unipolar, centertaps may be grounded, powered, or disconnected)
  These signals can be sent to the motor directly by the Arduino if the motor is small enough
  (If center tap is powered on Unipolar motors, don't exceed 5V, which is the Arduino's logic level)
  (Always check that things make sense that that you won't be driving too much current through the Arduino,
  the motor, or any amplfiers/drivers you use)
  For larger motors, inverting or Non-Inverting Darlington Arrays may be used to amplify these signals.
 
        CONCEPTUAL DIAGRAMS
     _________________               ________D________
    |                 |             |                 |
  A |--3              |             |                 |
    |  3              |             |    _____|\      |
    |--3     O        |           B |   |_____  )     | A
    |  3              |             |         |/      |
  B |--3              |             |                 |
    |   00000000000   |             |_________________|
    |___|____|____|___|                      C
        C         D     

        WAVE DRIVE:     FULL STEP DRIVE:    HALF STEP DRIVE:
 Step|  A  B  C  D   |    A  B  C  D     |    A  B  C  D    
Index|-------------------------------------------------------
    0|  1  0  0  0   |    1  0  1  0     |    1  0  0  0
    1|                                        1  0  1  0
    2|  0  0  1  0   |    0  1  1  0     |    0  0  1  0
    3|                                        0  1  1  0
    4|  0  1  0  0   |    0  1  0  1     |    0  1  0  0
    5|                                        0  1  0  1
    6|  0  0  0  1   |    1  0  0  1     |    0  0  0  1
    7|                                        1  0  0  1
     |-------------------------------------------------------
    
                                        
  FOR 2 WIRES: (Full Step Only)
  Note that full step drive always has A inverted with respect to B
  and C inverted with respect to D. Using logic inversion outside of the
  Arduino, we provide for full step drive with only 2 wires.
 
       FULL STEP DRIVE:
  Step| (�A,B)   (C,�D)
 Index|----------------
     0|    0        1
     2|    1        1
     4|    1        0
     6|    0        0

  Additional information on stepper motors may be found at 
  http://en.wikipedia.org/wiki/Stepper_motor
  http://www.tigoe.net/pcomp/code/circuits/motors/stepper-motors/
  
*/

#include "Arduino.h"
#include "GStepper.h"

//Constructors
GStepper::GStepper(int pin_1, int pin_2)
{
    current_pos = 0;       // In half-steps
    target_pos = 0;      // In half-steps
    last_step_time = 0;    // Time stamp in ms of the last step taken
    step_index = 0;      // The first few steps will be off. Recommend
    // writing function to move forward a full cycle
    // and back a full cycle and reset current position to 0.
    step_mode = 1;       // Default to full step drive
    ext_inv = true;

    motor_pin_1 = pin_1;
    motor_pin_2 = pin_2;

    pinMode(motor_pin_1, OUTPUT);
    pinMode(motor_pin_2, OUTPUT);
}

GStepper::GStepper(int pin_1, int pin_2, int pin_3, int pin_4)
{
    current_pos = 0;       // In half-steps
    target_pos = 0;      // In half-steps
    last_step_time = 0;    // Time stamp in ms of the last step taken
    step_index = 0;      // The first few steps will be off. Recommend
    // writing function to move forward a full cycle
    // and back a full cycle and reset current position to 0.
    step_mode = 1;       // Default to full step drive
    ext_inv = false;

    motor_pin_1 = pin_1;
    motor_pin_2 = pin_2;
    motor_pin_3 = pin_3;
    motor_pin_4 = pin_4;

    pinMode(motor_pin_1, OUTPUT);
    pinMode(motor_pin_2, OUTPUT);
    pinMode(motor_pin_3, OUTPUT);
    pinMode(motor_pin_4, OUTPUT);
}

// to move to a position at a given speed
// return false if we're already there
bool GStepper::move()
{
    if (target_pos == current_pos || (step_mode != 2 && target_pos - 1 == current_pos))
        return false;
    float time = millis();
    if (time >= last_step_time + delta_time)
    {
        step(sgn(target_pos - current_pos));
        last_step_time = time;
    }
    return true;
}

void GStepper::spin() // intended to be used for timed moves at a specific velocity
{
    float time = millis();
    if (time >= last_step_time + delta_time)
    {
        step(sgn(velocity));
        last_step_time = time;
    }
}

void GStepper::setSpeed(float set)
{
    setVelocity(set);
}
// yes, setSpeed and setVelocity are the same,
// but there's a conceptual difference
// and we get away with it because move()
// automatically corrects for sign

void GStepper::setVelocity(float set)
{
    if (set != 0) {
        velocity = set;
        if (step_mode == 2)
        delta_time = 1000/abs(velocity);
        else delta_time = 2000/abs(velocity);
    }
}

void GStepper::setMode(int mode)
{
    if (not ext_inv) {
        step_mode = (mode % 3);
        if (step_mode == 2)
        delta_time = 1000/abs(velocity);
        else delta_time = 2000/abs(velocity);
    }
}

float GStepper::getSpeed()
{
    return abs(velocity);
}

float GStepper::getVelocity()
{
    return velocity;
}

int GStepper::getMode()
{
    return step_mode;
}

int GStepper::sgn(float x)
{
    return ((x > 0) - (x < 0));
}

void GStepper::step(int dir) // dir takes on -1, 0, and 1 values
{
    switch (step_mode) {
    case 0: // Wave Drive
        step_index = (step_index + 8 + 2*dir) % 8;
        switch (step_index) {
        case 0: case 1: // 1000
            digitalWrite(motor_pin_1, HIGH);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 2: case 3: // 0010
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, HIGH);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 4: case 5: //0100
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, HIGH);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 6: case 7: //0001
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, HIGH);
            break;
        }
        current_pos = (current_pos + 2*dir);
        break;
    case 1: // Full Step
        step_index = (step_index + 8 + 2*dir) % 8;
        if (ext_inv) // 2 wires
        {
            switch (step_index) {
            case 0: case 1: // 01
                digitalWrite(motor_pin_1, LOW);
                digitalWrite(motor_pin_2, HIGH);
                break;
            case 2: case 3: // 11
                digitalWrite(motor_pin_1, HIGH);
                digitalWrite(motor_pin_2, HIGH);
                break;
            case 4: case 5: // 10
                digitalWrite(motor_pin_1, HIGH);
                digitalWrite(motor_pin_2, LOW);
                break;
            case 6: case 7: // 00
                digitalWrite(motor_pin_1, LOW);
                digitalWrite(motor_pin_2, LOW);
                break;
            }
            ;
        }
        else  // 4 wires
        {
            switch (step_index) {
            case 0: case 1: // 1010
                digitalWrite(motor_pin_1, HIGH);
                digitalWrite(motor_pin_2, LOW);
                digitalWrite(motor_pin_3, HIGH);
                digitalWrite(motor_pin_4, LOW);
                break;
            case 2: case 3: // 0110
                digitalWrite(motor_pin_1, LOW);
                digitalWrite(motor_pin_2, HIGH);
                digitalWrite(motor_pin_3, HIGH);
                digitalWrite(motor_pin_4, LOW);
                break;
            case 4: case 5: // 0101
                digitalWrite(motor_pin_1, LOW);
                digitalWrite(motor_pin_2, HIGH);
                digitalWrite(motor_pin_3, LOW);
                digitalWrite(motor_pin_4, HIGH);
                break;
            case 6: case 7: // 1001
                digitalWrite(motor_pin_1, HIGH);
                digitalWrite(motor_pin_2, LOW);
                digitalWrite(motor_pin_3, LOW);
                digitalWrite(motor_pin_4, HIGH);
                break;
            }
        }
        current_pos = (current_pos + 2*dir);
        break;
    case 2: // Half-Step
        step_index = (step_index + 8 + dir) % 8;
        switch (step_index) {
        case 0: // 1010
            digitalWrite(motor_pin_1, HIGH);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, HIGH);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 1: // 0010
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, HIGH);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 2: // 0110
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, HIGH);
            digitalWrite(motor_pin_3, HIGH);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 3: // 0100
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, HIGH);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, LOW);
            break;
        case 4: // 0101
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, HIGH);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, HIGH);
            break;
        case 5: // 0001
            digitalWrite(motor_pin_1, LOW);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, HIGH);
            break;
        case 6: // 1001
            digitalWrite(motor_pin_1, HIGH);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, HIGH);
            break;
        case 7: // 1000
            digitalWrite(motor_pin_1, HIGH);
            digitalWrite(motor_pin_2, LOW);
            digitalWrite(motor_pin_3, LOW);
            digitalWrite(motor_pin_4, LOW);
            break;
        }
        current_pos = (current_pos + dir);
        break;
    }
}