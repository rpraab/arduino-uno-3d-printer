/*
  GStepper.h - by Reilly Raab
  The G stands for "Gaucho" - written at UCSB
  
  Inspired by Stepper library by Tom Igoe
  and AccelStepper by Mike McCauley
  
  LICENCED UNDER GNU GPL V2.0
  CONSPICUOUS NOTICE:
  NO EXPRESSED OR IMPLIED WARRANTY, COMES AS IS
  USE AT YOUR OWN RISK, ETC.
  FREE TO MODIFY AND REDISTRIBUTE, BUT MUST
  REMAIN UNDER GPL V2 AND PROVIDE COPY OF LICENCE

  Note, we measure everything in half-steps, since that's the smallest
  change in position we can easily get our system to make.
  
  FOR 4 WIRES:
  (Bipolar or Unipolar. For Unipolar, centertaps may be grounded, powered, or disconnected)
  These signals can be sent to the motor directly by the Arduino if the motor is small enough
  (If center tap is powered on Unipolar motors, don't exceed 5V, which is the Arduino's logic level)
  (Always check that things make sense that that you won't be driving too much current through the Arduino,
  the motor, or any amplfiers/drivers you use)
  For larger motors, inverting or Non-Inverting Darlington Arrays may be used to amplify these signals.
 
        CONCEPTUAL DIAGRAMS
     _________________               ________D________
    |                 |             |                 |
  A |--3              |             |                 |
    |  3              |             |    _____|\      |
    |--3     O        |           B |   |_____  )     | A
    |  3              |             |         |/      |
  B |--3              |             |                 |
    |   00000000000   |             |_________________|
    |___|____|____|___|                      C
        C         D     

        WAVE DRIVE:     FULL STEP DRIVE:    HALF STEP DRIVE:
 Step|  A  B  C  D   |    A  B  C  D     |    A  B  C  D    
Index|-------------------------------------------------------
    0|  1  0  0  0   |    1  0  1  0     |    1  0  1  0
    1|                                        0  0  1  0
    2|  0  0  1  0   |    0  1  1  0     |    0  1  1  0
    3|                                        0  1  0  0
    4|  0  1  0  0   |    0  1  0  1     |    0  1  0  1
    5|                                        0  0  0  1
    6|  0  0  0  1   |    1  0  0  1     |    1  0  0  1
    7|                                        1  0  0  0
     |-------------------------------------------------------

                                        
  FOR 2 WIRES: (Full Step Only)
  Note that full step drive always has A inverted with respect to B
  and C inverted with respect to D. Using logic inversion outside of the
  Arduino, we provide for full step drive with only 2 wires.
 
       FULL STEP DRIVE:
  Step| (�A,B)   (C,�D)
 Index|----------------
     0|    0        1
     2|    1        1
     4|    1        0
     6|    0        0

  Additional information on stepper motors may be found at 
  http://en.wikipedia.org/wiki/Stepper_motor
  http://www.tigoe.net/pcomp/code/circuits/motors/stepper-motors/
  
*/
 
#include <Arduino.h>
#ifndef GStepper_h
#define GStepper_h

class GStepper {
  public:
    //Constructors
    GStepper(int pin_1, int pin_2);
    GStepper(int pin_1, int pin_2, int pin_3, int pin_4);

    bool move(); 
    // Calls step() if it is time for a step to be made
    // returns FALSE and doesn't step when current_pos = target_pos
    
    void spin(); 
    // Calls step() if it is time for a step to be made
        
    void setSpeed(float set);    // In half-steps/second
    void setVelocity(float set); // Set magnitude AND direction of velocity
    void setMode(int mode);  // 0 Wave; 1 Full Step; 2 Half Step
                                 // sets step_mode to mode mod 3
                                 // can only be changed for 4 wire setups

    float getSpeed();    // In half-steps/second
    float getVelocity(); // In half-steps/second
    int getMode();   // 0 Wave; 1 Full Step; 2 Half Step
    
        // VARIABLES
    int current_pos;        // In half-steps
    int target_pos;         // In half-steps

  private:
    int sgn(float x);       // Returns -1 for negative, 0 for 0, 1 for positive
                            // Used for determining which direction we're moving with velocity as argument
    void step(int dir);     // steps the motor once forward or backwards
    
        // VARIABLES
    byte motor_pin_1;       // Self-explanatory
    byte motor_pin_2;       //
    byte motor_pin_3;       //
    byte motor_pin_4;       //

    bool ext_inv;           // external inversion? True for 2 wires, false for 4
    
    int step_mode;          // 0 Wave; 1 Full Step; 2 Half Step
    int step_index;         // Values 0 through 7. Has to be independent
                            // of current_pos since 
                            // we allow the user to set the latter.

    float velocity;         // In half-steps per second, sign matters
    float delta_time;       // ms between pin output changes, depends on step mode
    float last_step_time;   // time stamp in ms of when the last step was taken


};

#endif

