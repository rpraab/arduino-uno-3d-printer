/*
  GStepper, by Reilly Raab
  The G stands for "Gaucho" - written at UCSB 2013
  
  Inspired by Stepper library by Tom Igoe
  and AccelStepper by Mike McCauley
  
  LICENCED UNDER GNU GPL V2.0
  CONSPICUOUS NOTICE:
  NO EXPRESSED OR IMPLIED WARRANTY, COMES AS IS
  USE AT YOUR OWN RISK, ETC.
  FREE TO MODIFY AND REDISTRIBUTE, BUT MUST
  REMAIN UNDER GPL V2 AND PROVIDE COPY OF LICENCE

  Note, we measure everything in half-steps, since that's the smallest
  change in position we can easily get our system to make.
  
  FOR 4 WIRES:
  (Bipolar or Unipolar. For Unipolar, centertaps may be grounded, powered, or disconnected)
  These signals can be sent to the motor directly by the Arduino if the motor is small enough
  (If center tap is powered on Unipolar motors, don't exceed 5V, which is the Arduino's logic level)
  (Always check that things make sense that that you won't be driving too much current through the Arduino,
  the motor, or any amplfiers/drivers you use)
  For larger motors, inverting or Non-Inverting Darlington Arrays may be used to amplify these signals.
 
        CONCEPTUAL DIAGRAMS
     _________________               ________D________
    |                 |             |                 |
  A |--3              |             |                 |
    |  3              |             |    _____|\      |
    |--3     O        |           B |   |_____  )     | A
    |  3              |             |         |/      |
  B |--3              |             |                 |
    |   00000000000   |             |_________________|
    |___|____|____|___|                      C
        C         D     

        WAVE DRIVE:     FULL STEP DRIVE:    HALF STEP DRIVE:
 Step|  A  B  C  D   |    A  B  C  D     |    A  B  C  D    
Index|-------------------------------------------------------
    0|  1  0  0  0   |    1  0  1  0     |    1  0  1  0
    1|                                        0  0  1  0
    2|  0  0  1  0   |    0  1  1  0     |    0  1  1  0
    3|                                        0  1  0  0
    4|  0  1  0  0   |    0  1  0  1     |    0  1  0  1
    5|                                        0  0  0  1
    6|  0  0  0  1   |    1  0  0  1     |    1  0  0  1
    7|                                        1  0  0  0
     |-------------------------------------------------------

                                        
  FOR 2 WIRES: (Full Step Only)
  Note that full step drive always has A inverted with respect to B
  and C inverted with respect to D. Using logic inversion outside of the
  Arduino, we provide for full step drive with only 2 wires.
 
       FULL STEP DRIVE:
  Step| (¬A,B)   (C,¬D)
 Index|----------------
     0|    0        1
     2|    1        1
     4|    1        0
     6|    0        0

  Additional information on stepper motors may be found at 
  http://en.wikipedia.org/wiki/Stepper_motor
  http://www.tigoe.net/pcomp/code/circuits/motors/stepper-motors/
  
*/
#include <GStepper.h>

GStepper x(2,3,4,5); // S

void setup()
{
}

void loop()
{
    x.target_pos =(rand() % 200);
    x.setSpeed((rand() % 200) + 1);
    while(x.move())
    {
      ; // just calls move() until it returns false
    }
}
