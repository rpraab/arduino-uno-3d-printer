// Random.pde
// -*- mode: C++ -*-
//
// Make a single stepper perform random changes in speed, position and acceleration
//
// Copyright (C) 2009 Mike McCauley
// $Id: HRFMessage.h,v 1.1 2009/08/15 05:32:58 mikem Exp mikem $

#include <GStepper.h>

// Define a stepper and the pins it will use
GStepper stepper(2,3,4,5); // Defaults to 4 pins on 2, 3, 4, 5

void setup()
{
  stepper.setMode(2);
  Serial.begin(9600);
}

void loop()
{
    stepper.target_pos =(rand() % 200);
    stepper.setSpeed((rand() % 200) + 1);
    Serial.print("target: ");
    Serial.println(stepper.target_pos);
    while(stepper.move())
    {
      Serial.println(stepper.current_pos);
      ; // just calls move() until it returns false
    }
}
