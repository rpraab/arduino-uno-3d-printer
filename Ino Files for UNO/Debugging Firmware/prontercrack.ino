// Chocolate 3D Printer Firmware
// Reilly Raab 2013 UCSB

void setup() {
  // pinMode(A0, OUTPUT); // Just know that we can do this on an analog input pin!
  
  // start serial port at 9600 bps and wait for port to open:
  Serial.begin(9600);
  while (Serial.available() <= 0) {
    Serial.print("ok");
    delay(1000);
  }
}

void loop() {
  while (Serial.available() > 0) {
    char nextbyte = Serial.read();
    if (nextbyte == '\n') {
      Serial.print("\nok");
    }
    Serial.print(nextbyte);
  }
}

