/*
Arduino G-code Interpreter
v1.0 - 1.3 by Mike Ellery (mellery@gmail.com), Zach Hoeken (hoeken@gmail.com), and Chris Meighan (cmeighan@gmail.com)
v2.0 by Reilly Raab (rpraab@umail.ucsb.edu)
    Reworked stepper driver control to support 2 wire drive with Darlington Arrays
    by writing GStepper library. Cleaned up Serial communication. Rewrote most of G-Code case structure.
    Written for Arduino UNO driving 4 stepper motors with 2 wires each for X,Y,Z, and Extruder
    Should be trivial to change to 4-wire control of stepper motors. Just instantiate
    GStepper class with 4 arguments rather than 2.
*/

#include <GStepper.h>

// define the parameters of our machine.
#define X_STEPS_PER_MM      14.035
#define Y_STEPS_PER_MM      13.7363
#define Z_STEPS_PER_MM      64
#define E_STEPS_PER_MM      50

GStepper X(6,7);
//GStepper X(2,3,4,5);
GStepper Y(4,5);
//GStepper Y(6,7,8,9);
GStepper Z(2,3);
//GStepper Z(2,3,4,5);
GStepper E(8,9);
//GStepper E(6,7,8,9);


#define BUFFER_SIZE 128
//for Arduino UNO

char buffer[BUFFER_SIZE]; //rather than instantiate a new buffer with each command read
byte serial_count;

void setup()
{
    Serial.begin(19200);
    Serial.println("Start");
    
    Z.setSpeed(5*64);
    
    //other initialization.
    clearBuffer();
    //init_extruder();
}

void loop()
{
    //extruder_manage_temperature();

    if (readCommand())
    {
        //process our command!
        //Serial.print(deltaX);
        //Serial.print(" ");
        process_string(buffer, serial_count);
        //Serial.println(deltaY);
        clearBuffer();
        
        while (executeMotion())
        {
            ;
        }
    }
}
