float Xunits = X_STEPS_PER_MM;
float Yunits = Y_STEPS_PER_MM;
float Zunits = Z_STEPS_PER_MM;
float Eunits = E_STEPS_PER_MM;

float feedrate = 2; // units per second

float deltaX = 0;
float deltaY = 0;
float deltaZ = 0;
float deltaE = 0;

float OMEGA; // 1/time of move in E

void calculateEXYSpeeds()
{
    OMEGA = feedrate/deltaE*Eunits;
    X.setSpeed(OMEGA*deltaX*Xunits);
    Y.setSpeed(OMEGA*deltaY*Yunits);
}

bool executeMotion() // want to be true until we're done moving
{
    return (X.move() + Y.move() + Z.move() + E.move());
}
