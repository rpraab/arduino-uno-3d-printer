/*
Nothing particularly fancy here.
No circular arcs or anything like that.
For comprehensive G-Code parsing, take a look at GRBL
http://dank.bengler.no/-/page/show/5470_grbl

Parsing of G-Code here is limited
to G0 and G1 moves (Slic3r uses exclusively
G1 for generation of RepRap code), setting units,
homing motors, dwell, and switching between
incremental and absolute positioning.
*/

//Default to absolute units, metric
boolean abs_mode = true;   //0 = incremental; 1 = absolute
boolean metric = true; //0 = inches; 1 = metric.

// set serial_count to 0, clear buffer
void clearBuffer()
{
    for (byte i=0; i<BUFFER_SIZE; i++)
        buffer[i] = 0;
    serial_count = 0;
}

// read serial input into buffer a character at a time
// Ignore everything on a line following a '/' or a ';'
// stop, send "ok", and return true when we get a new line
bool readCommand() 
{
    char c;
    
    while (Serial.available() > 0)
    {
        c = Serial.read();
        
        if (c == '/' || c == ';')
        {
            while (c != '\n')
            {
                c = Serial.read();
            }
        }
        if (c == '\n')
        {
            Serial.println("ok");
            return true;
        }
        
        buffer[serial_count] = c;
        serial_count++;
    }

    return false;
}

//Read the string and execute instructions
void process_string(char instruction[], int size)
{

	byte code = 0;
	
	if (has_command('G', instruction, size))
	{
		//which one?
		code = (int)search_string('G', instruction, size);
		
		switch (code)
		{
            case 0: // Slic3r only uses G1
			case 1: // Controlled Move
				if(abs_mode)
				{
					if (has_command('X', instruction, size))
						X.target_pos = search_string('X', instruction, size)*Xunits;
                        deltaX = (X.target_pos - X.current_pos)/Xunits;
					if (has_command('Y', instruction, size))
						Y.target_pos = search_string('Y', instruction, size)*Yunits;
                        deltaY = (Y.target_pos - Y.current_pos)/Yunits;
					if (has_command('Z', instruction, size))
						Z.target_pos = search_string('Z', instruction, size)*Zunits;
                        deltaZ = (Z.target_pos - Z.current_pos)/Zunits;
                    if (has_command('E', instruction, size))
                        E.target_pos = search_string('E', instruction, size)*Eunits;
                        deltaE = (E.target_pos - E.current_pos)/Eunits; 
				}
				else
				{
                    // I think this rounds float down to convert to int? We'll have systematic error of less than a step per move.
                    deltaX = search_string('X', instruction, size);
                    deltaY = search_string('Y', instruction, size);
                    deltaZ = search_string('Z', instruction, size);
                    deltaE = search_string('E', instruction, size);
					X.target_pos = X.current_pos + deltaX*Xunits;
					Y.target_pos = Y.current_pos + deltaY*Yunits;
					Z.target_pos = Z.current_pos + deltaZ*Zunits;
                    E.target_pos = E.current_pos + deltaE*Eunits;
				}
                
                if (has_command('R', instruction, size))
                {
                    float fr = search_string('R', instruction, size);
                    if (fr != 0)
                        feedrate = fr;
                }
                
                
                if (has_command('E', instruction, size))
				{
                        calculateEXYSpeeds();
				}
                else
                {
                        X.setSpeed(30*Xunits);
                        Y.setSpeed(30*Yunits);
                }
            
			//Inches for Units
			case 20:
                if (metric) {
                    Xunits = Xunits*2.54;
                    Yunits = Yunits*2.54;
                    Zunits = Zunits*2.54;
                    Eunits = Eunits*2.54;
                    feedrate = feedrate/2.54;
                    metric = false;
                }
			break;

			//mm for Units
			case 21:
                if (not metric) {
                    Xunits = Xunits/2.54;
                    Yunits = Yunits/2.54;
                    Zunits = Zunits/2.54;
                    Eunits = Eunits/2.54;
                    feedrate = feedrate*2.54;
                    metric = true;
                }

			//Absolute Positioning
			case 90:
				abs_mode = true;
			break;

			//Incremental Positioning
			case 91:
				abs_mode = false;
			break;

            case 92: // set position
                if (has_command('X', instruction, size))
                    X.current_pos = search_string('X', instruction, size)*Xunits;
                    X.target_pos = X.current_pos;
                if (has_command('Y', instruction, size))
                    Y.current_pos = search_string('Y', instruction, size)*Yunits;
                    Y.target_pos = Y.current_pos;
                if (has_command('Z', instruction, size))
                    Z.current_pos = search_string('Z', instruction, size)*Zunits;
                    Z.target_pos = Z.current_pos;
                if (has_command('E', instruction, size))
                    E.current_pos = search_string('E', instruction, size)*Eunits;
                    E.target_pos = E.current_pos;
            break;
            
			default:
				Serial.print("Unrecognized Command: G");
				Serial.println(code,DEC);
            break;
		}
	}

	
	if (has_command('M', instruction, size))
	{
		code = search_string('M', instruction, size); // defaults to 0 if no number given
		switch (code) 
		{            
			default:
				Serial.print("Unrecognized Command: M");
				Serial.println(code);
            break;
		}		
	}

}

//look for the number that appears after the char key and return it as a double
//the Arduino parseFloat() function loses sign information http://arduino.cc/en/Reference/StreamParseFloat
//returns 0 if nothing found
double search_string(char key, char instruction[], int string_size)
{
	char temp[10] = "";

	for (byte i=0; i<string_size; i++)
	{
		if (instruction[i] == key)
		{
			i++;      
			int k = 0;
			while (i < string_size && k < 10)
			{
				temp[k] = instruction[i];
				i++;
				k++;
			}
			return strtod(temp, NULL);
		}
	}
    return 0;
}

//look for the command if it exists.
bool has_command(char key, char instruction[], int string_size)
{
	for (byte i=0; i<string_size; i++)
	{
		if (instruction[i] == key)
			return true;
	}
	
	return false;
}
