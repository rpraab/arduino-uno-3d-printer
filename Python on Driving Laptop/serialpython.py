import serial

f = open('sample.gcode', mode="r")
print f

cxn = serial.Serial("COM3", 9600)
cxn.flushInput()
cxn.flushOutput()

connected = False
while not connected:
    if (cxn.readline() == "?\r\n"):
        cxn.flushInput()
        cxn.write("!")
        if (cxn.readline() == "!\r\n"):
            print "Connection Established"
            connected = True
            break

for line in f:
    while True:
        read = cxn.readline()
        if (read == "...\r\n"):
            cxn.write(line)
            print(line)
            break
        else:
            print read

cxn.write("Done")
print "Success!"

cxn.close()

"""
To do:

Start with intial buffer, see how long until "Waiting for Data" is returned
Better yet, instead of having the Arduino send "...\r\n", have it send serial.available()

Want Pause Button - need GUI for that
Record position (line number) in file as read to resume from pause, also z position
Want Abort Button - again need GUI
Home Motors
Manual Move x,y,z
Manual function calls to arduino

Get encoders working. Get motors working.
Write fucntion in arduino to move xy with velocity control and extrude.

Rewrite with function calls?
"""
        

    
    
    
