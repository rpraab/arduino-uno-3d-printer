# README #

Code that was used in 2013 for an Arduino-UNO powered 3D printer.

The extruder had no active temperature management, and streaming G-Code to the device
was accomplished with Printrun for very simple move sequences only (a very limited subset of G-Code commands was supported by the firmware) . A copy of Printrun as it appeared in 2013 is within this repository.

The library called "GStepper" is probably the most useful thing here, and was used to control the stepper motors. This repository exists mostly to preserve this single library, as well as to suggest a minimal context in which it might be useful.

This code is unmaintained and possibly incomplete.